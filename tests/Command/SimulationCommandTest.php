<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Exception\FileNotFoundException;
use App\Exception\InvalidConfigurationException;
use App\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CleanRoomCommandTest
 *
 * @package App\Tests\Command
 */
class SimulationCommandTest extends KernelTestCase
{
    public function testSuccessSimulation(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('simulation:start');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            [
                'input' => 'input_test.json',
                'output' => 'output_test.json',
            ]
        );

        $output = $commandTester->getDisplay();
        self::assertStringContainsString('Command success', $output);
    }

    public function testWall(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('simulation:start');
        $commandTester = new CommandTester($command);
        $this->expectException(ValidationException::class);
        $commandTester->execute(
            [
                'input' => 'wall_test.json',
                'output' => 'output_test.json',
            ]
        );
    }

    public function testNegativeCoordinates(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('simulation:start');
        $commandTester = new CommandTester($command);
        $this->expectException(ValidationException::class);
        $commandTester->execute(
            [
                'input' => 'negative_test.json',
                'output' => 'output_test.json',
            ]
        );
    }

    public function testBattery(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('simulation:start');
        $commandTester = new CommandTester($command);
        $this->expectException(ValidationException::class);
        $commandTester->execute(
            [
                'input' => 'battery_test.json',
                'output' => 'output_test.json',
            ]
        );
    }

    public function testInvalidFile(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('simulation:start');
        $commandTester = new CommandTester($command);
        $this->expectException(InvalidConfigurationException::class);
        $commandTester->execute(
            [
                'input' => 'invalid_test.json',
                'output' => 'output_test.json',
            ]
        );
    }

    public function testNonExistingFile(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('simulation:start');
        $commandTester = new CommandTester($command);
        $this->expectException(FileNotFoundException::class);
        $commandTester->execute(
            [
                'input' => 'non_existing_test.json',
                'output' => 'output_test.json',
            ]
        );
    }
}
