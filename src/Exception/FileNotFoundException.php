<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

/**
 * Class FileNotFoundException
 *
 * @package App\Exception
 */
class FileNotFoundException extends Exception
{
    public function __construct(string $filename = "", $code = 0, Throwable $previous = null)
    {
        $message = sprintf("File `%s` not found in the storage directory", $filename);
        parent::__construct($message, $code, $previous);
    }
}
