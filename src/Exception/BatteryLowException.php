<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class BatteryLowException
 *
 * @package App\Exception
 */
class BatteryLowException extends Exception
{

}
