<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class StuckException
 *
 * @package App\Exception
 */
class StuckException extends Exception
{

}
