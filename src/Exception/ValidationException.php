<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class ValidationException
 *
 * @package App\Exception
 */
class ValidationException extends Exception
{

}
