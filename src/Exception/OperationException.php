<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class OperationException
 *
 * @package App\Exception
 */
class OperationException extends Exception
{

}
