<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidConfigurationException
 *
 * @package App\Exception
 */
class InvalidConfigurationException extends Exception
{
    public function __construct(string $json = "", $code = 0, Throwable $previous = null)
    {
        $message = sprintf("Given configuration file is not in valid format: %s", $json);
        parent::__construct($message, $code, $previous);
    }
}
