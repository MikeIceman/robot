<?php

declare(strict_types=1);

namespace App\Factory;

use App\Component\Cell;
use App\Component\Config;
use App\Domain\Factory\ConfigFactoryInterface;
use App\Exception\InvalidConfigurationException;
use App\Exception\ValidationException;
use Exception;
use Karriere\JsonDecoder\JsonDecoder;
use Psr\Log\LoggerInterface;

/**
 * Class ConfigFactory
 *
 * @package App\Factory
 */
class ConfigFactory implements ConfigFactoryInterface
{
    /** @var LoggerInterface */
    private LoggerInterface $logger;

    /** @var JsonDecoder */
    private JsonDecoder $decoder;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->decoder = new JsonDecoder();
    }

    /**
     * @param string $json
     *
     * @return Config
     * @throws InvalidConfigurationException
     * @throws ValidationException
     */
    public function parseConfig(string $json): Config
    {
        try {
            $this->decoder->scanAndRegister(Config::class);
            /** @var Config $config */
            $config = $this->decoder->decode($json, Config::class);
            $start = $config->getStart();
            if ($start->getX() < 0 || $start->getY() < 0) {
                throw new ValidationException(
                    'Start coordinates must be positive. X: '.$start->getX().' Y:' . $start->getY() . ' given'
                );
            }
            $cell = $config->getCell($start->getX(), $start->getY());
            if ($cell->getType() !== Cell::CLEANABLE) {
                throw new ValidationException(
                    'Robot can not start inside the wall. X: '.$start->getX().' Y:' . $start->getY() . ''
                );
            }
            return $config;
        } catch (ValidationException $ex) {
            $this->logger->debug($ex->getMessage());
            throw $ex;
        } catch (Exception $ex) {
            $this->logger->debug($ex->getMessage());
            throw new InvalidConfigurationException($json, $ex->getCode(), $ex->getPrevious());
        }
    }
}
