<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController
{
    /**
      * @Route("/")
      */
    public function index(): Response
    {
        return new Response(
            '<html><head><title>Hello World!</title></head><body>Default index</body></html>'
        );
    }
}
