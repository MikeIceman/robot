<?php

declare(strict_types=1);

namespace App\Domain\Component;

/**
 * Interface CommandInterface
 *
 * @package App\Domain\Component
 */
interface CommandInterface
{
    /**
     * @param string $command
     *
     * @return int
     */
    public static function getCost(string $command): int;
}
