<?php

declare(strict_types=1);

namespace App\Domain\Component;

/**
 * Interface PositionInterface
 *
 * @package App\Domain\Component
 */
interface PositionInterface
{
    /**
     * @return int
     */
    public function getX(): int;

    /**
     * @param int $X
     */
    public function setX(int $X): void;

    /**
     * @return int
     */
    public function getY(): int;

    /**
     * @param int $Y
     */
    public function setY(int $Y): void;

    /**
     * @return string
     */
    public function getFacing(): string;

    /**
     * @param string $facing
     */
    public function setFacing(string $facing): void;
}
