<?php

declare(strict_types=1);

namespace App\Domain\Component;

use App\Component\Cell;
use App\Component\Position;
use App\Exception\BatteryLowException;
use App\Exception\ValidationException;

/**
 * Interface RobotInterface
 *
 * @package App\Domain\Component
 */
interface RobotInterface
{
    /**
     * @return Position
     */
    public function getPosition(): Position;

    /**
     * @param Position $position
     */
    public function setPosition(Position $position): void;

    /**
     * @return int
     */
    public function getBattery(): int;

    /**
     * @param int $battery
     *
     * @throws ValidationException
     */
    public function setBattery(int $battery): void;

    /**
     * @param int $value
     *
     * @throws BatteryLowException
     */
    public function consume(int $value): void;

    /**
     * Turns robot to left
     *
     * @throws BatteryLowException
     */
    public function turnLeft(): bool;

    /**
     * Turns robot to right
     *
     * @throws BatteryLowException
     */
    public function turnRight(): bool;

    /**
     * @param Cell $cell
     * @param string $command
     *
     * @return bool
     * @throws BatteryLowException
     */
    public function move(Cell $cell, string $command): bool;

    /**
     * @return bool
     * @throws BatteryLowException
     */
    public function clean(): bool;
}
