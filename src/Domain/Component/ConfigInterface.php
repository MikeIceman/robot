<?php

declare(strict_types=1);

namespace App\Domain\Component;

use App\Component\Cell;
use App\Component\Position;

/**
 * Interface ConfigInterface
 *
 * @package App\Domain\Component
 */
interface ConfigInterface
{
    /**
     * @param int $x
     * @param int $y
     *
     * @return Cell
     */
    public function getCell(int $x, int $y): Cell;

    /**
     * @return Position
     */
    public function getStart(): Position;

    /**
     * @return string[]
     */
    public function getCommands(): array;

    /**
     * @return int
     */
    public function getBattery(): int;
}
