<?php

declare(strict_types=1);

namespace App\Domain\Component;

/**
 * Interface CellInterface
 *
 * @package App\Domain\Component
 */
interface CellInterface
{
    /**
     * @return int
     */
    public function getX(): int;

    /**
     * @param int $X
     */
    public function setX(int $X): void;

    /**
     * @return int
     */
    public function getY(): int;

    /**
     * @param int $Y
     */
    public function setY(int $Y): void;

    /**
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void;
}
