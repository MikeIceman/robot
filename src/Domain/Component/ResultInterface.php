<?php

declare(strict_types=1);

namespace App\Domain\Component;

use App\Component\Cell;
use App\Component\Position;

/**
 * Interface ResultInterface
 *
 * @package App\Domain\Component
 */
interface ResultInterface
{
    /**
     * @param Position $final
     */
    public function setFinal(Position $final): void;

    /**
     * @param int $battery
     */
    public function setBattery(int $battery): void;

    /**
     * @param Cell $cell
     */
    public function addVisited(Cell $cell): void;

    /**
     * @param Cell $cell
     */
    public function addCleaned(Cell $cell): void;
}
