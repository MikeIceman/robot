<?php

declare(strict_types=1);

namespace App\Domain\Component;

/**
 * Interface FacingInterface
 *
 * @package App\Domain\Component
 */
interface FacingInterface
{
    /**
     * @param string $command
     *
     * @return int[]
     */
    public function getMoveSet(string $command): array;
}
