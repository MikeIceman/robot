<?php

declare(strict_types=1);

namespace App\Domain\Factory;

use App\Component\Config;
use App\Exception\InvalidConfigurationException;
use App\Exception\ValidationException;

/**
 * Interface ConfigFactoryInterface
 *
 * @package App\Domain\Factory
 */
interface ConfigFactoryInterface
{
    /**
     * @param string $json
     *
     * @return Config
     * @throws InvalidConfigurationException
     * @throws ValidationException
     */
    public function parseConfig(string $json): Config;
}
