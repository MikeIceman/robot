<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Exception\FileNotFoundException;

/**
 * Interface FileStorageServiceInterface
 *
 * @package App\Domain\Service
 */
interface FileStorageServiceInterface
{
    /**
     * @param string $filename
     *
     * @return string
     * @throws FileNotFoundException
     */
    public function getFileContents(string $filename): string;

    /**
     * @param string $contents
     * @param string $filename
     *
     * @return bool
     */
    public function storeFile(string $contents, string $filename): bool;

    /**
     * @param mixed $object
     * @param string $filename
     *
     * @return bool
     */
    public function storeObject($object, string $filename): bool;
}
