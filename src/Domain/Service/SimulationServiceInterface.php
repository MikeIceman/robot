<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Component\Config;
use App\Domain\Component\ResultInterface;
use App\Exception\ValidationException;

/**
 * Interface SimulationServiceInterface
 *
 * @package App\Domain\Service
 */
interface SimulationServiceInterface
{
    /**
     * Simulate robot movements with given configuration
     *
     * @param Config $config
     *
     * @return ResultInterface
     * @throws ValidationException
     */
    public function simulate(Config $config): ResultInterface;
}
