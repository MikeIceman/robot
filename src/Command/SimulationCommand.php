<?php

declare(strict_types=1);

namespace App\Command;

use App\Domain\Factory\ConfigFactoryInterface;
use App\Domain\Service\FileStorageServiceInterface;
use App\Domain\Service\SimulationServiceInterface;
use App\Exception\FileNotFoundException;
use App\Exception\InvalidConfigurationException;
use App\Exception\ValidationException;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CleanRoomCommand
 *
 * @package App\Command
 */
class SimulationCommand extends Command
{
    /** @var SimulationServiceInterface */
    private SimulationServiceInterface $simulationService;

    /** @var FileStorageServiceInterface */
    private FileStorageServiceInterface $fileStorageService;

    /** @var ConfigFactoryInterface */
    private ConfigFactoryInterface $configFactory;

    /** @var LoggerInterface */
    private LoggerInterface $logger;

    public function __construct(
        SimulationServiceInterface $simulationService,
        LoggerInterface $logger,
        FileStorageServiceInterface $fileStorageService,
        ConfigFactoryInterface $configFactory
    ) {
        $this->simulationService = $simulationService;
        $this->fileStorageService = $fileStorageService;
        $this->configFactory = $configFactory;
        $this->logger = $logger;
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws ValidationException
     * @throws FileNotFoundException
     * @throws InvalidConfigurationException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(
            [
                '===================',
                'Starting simulation',
                '===================',
                '',
                'Input file name: ' . $input->getArgument('input'),
                'Output file name: ' . $input->getArgument('output'),
                '',
            ]
        );

        try {
            $config = $this->configFactory->parseConfig(
                $this->fileStorageService->getFileContents($input->getArgument('input'))
            );
            $result = $this->simulationService->simulate($config);
            $this->fileStorageService->storeObject($result, $input->getArgument('output'));
            $output->writeln(
                [
                    '',
                    '===============',
                    'Command success',
                    '===============',
                ]
            );
            return Command::SUCCESS;
        } catch (ValidationException $ex) {
            $this->logger->error(sprintf("Program finished because of validation exception: %s", $ex->getMessage()));
            throw $ex;
        } catch (Exception $ex) {
            $this->logger->error(sprintf("Error while execution has occurred: %s", $ex->getMessage()));
            throw $ex;
        }
    }

    protected function configure(): void
    {
        $this->setName('simulation:start')
             ->setDescription(
                 'A CLI command that will receive a json file as an input with the parameter specified and will run the robot simulation and produce the output'
             )
             ->addArgument(
                 'input',
                 InputArgument::REQUIRED,
                 'Input file name'
             )
             ->addArgument(
                 'output',
                 InputArgument::REQUIRED,
                 'Output file name'
             );
    }
}
