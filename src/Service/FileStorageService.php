<?php

declare(strict_types=1);

namespace App\Service;

use App\Domain\Service\FileStorageServiceInterface;
use App\Exception\FileNotFoundException;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FileStorageService
 *
 * @package App\Service
 */
class FileStorageService implements FileStorageServiceInterface
{
    /** @var Filesystem */
    private Filesystem $filesystem;

    /** @var LoggerInterface */
    private LoggerInterface $logger;

    /** @var string */
    private string $path;

    public function __construct(
        Filesystem $filesystem,
        LoggerInterface $logger,
        ParameterBagInterface $params
    ) {
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->setPath($params->get('storage_directory'));
        $this->initDirectory();
    }

    /**
     * @param string $filename
     *
     * @return string
     * @throws FileNotFoundException
     */
    public function getFileContents(string $filename): string
    {
        $filePath = $this->path . DIRECTORY_SEPARATOR . $filename;
        if ($this->filesystem->exists($filePath)) {
            return file_get_contents($filePath);
        }
        throw new FileNotFoundException($filename);
    }

    /**
     * @param string $contents
     * @param string $filename
     *
     * @return bool
     */
    public function storeFile(string $contents, string $filename): bool
    {
        if ($this->checkDirectory()) {
            $filePath = $this->path . DIRECTORY_SEPARATOR . $filename;
            $this->filesystem->dumpFile($filePath, $contents);
            return true;
        }
        return false;
    }

    /**
     * @param mixed $object
     * @param string $filename
     *
     * @return bool
     * @throws JsonException
     */
    public function storeObject($object, string $filename): bool
    {
        return $this->storeFile(json_encode($object, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT), $filename);
    }

    /**
     * Make directory for exported files
     */
    private function initDirectory(): void
    {
        $this->filesystem->mkdir($this->getPath());
        if (!is_dir($this->getPath())) {
            throw new RuntimeException(sprintf('Failed to create directory "%s"', $this->getPath()));
        }
    }

    /**
     * @return bool
     */
    public function checkDirectory(): bool
    {
        if (!is_writable($this->getPath())) {
            $this->logger->error(sprintf("Directory '%s' is not writable", $this->getPath()));
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}
