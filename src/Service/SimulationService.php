<?php

declare(strict_types=1);

namespace App\Service;

use App\Component\Command;
use App\Component\Config;
use App\Component\Facing;
use App\Component\Result;
use App\Component\Robot;
use App\Domain\Component\ResultInterface;
use App\Domain\Component\RobotInterface;
use App\Domain\Service\SimulationServiceInterface;
use App\Exception\BatteryLowException;
use App\Exception\StuckException;
use App\Exception\ValidationException;
use Assert\Assertion;
use Psr\Log\LoggerInterface;

/**
 * Class SimulationService
 *
 * @package App\Service
 */
class SimulationService implements SimulationServiceInterface
{
    /** @var RobotInterface */
    private RobotInterface $robot;

    /** @var ResultInterface */
    private ResultInterface $result;

    /** @var Config */
    private Config $config;

    /** @var LoggerInterface */
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, RobotInterface $robot, ResultInterface $result)
    {
        $this->logger = $logger;
        $this->robot = $robot;
        $this->result = $result;
    }

    /**
     * @param Config $config
     *
     * @return Result
     * @throws ValidationException
     */
    public function simulate(Config $config): ResultInterface
    {
        $this->config = $config;
        $this->robot->setPosition($config->getStart());
        $this->robot->setBattery($config->getBattery());
        try {
            $this->handleQueue($config->getCommands());
        } catch (BatteryLowException $ex) {
            $this->logger->warning('Battery low. Finishing');
        } catch (StuckException $ex) {
            $this->logger->warning('The robot is considered stuck. Finishing');
        }
        $this->result->setBattery($this->robot->getBattery());
        $this->result->setFinal($this->robot->getPosition());

        return $this->result;
    }

    /**
     * @param string[] $commands
     *
     * @throws BatteryLowException
     * @throws StuckException
     */
    private function handleQueue(array $commands): void
    {
        Assertion::allString($commands);

        foreach ($commands as $command) {
            Command::assertValidValue($command);
            if (!$this->performAction($command)) {
                $this->startBackOffStrategy();
            }
        }
    }

    /**
     * @param string $action
     *
     * @return bool
     * @throws BatteryLowException
     */
    private function performAction(string $action): bool
    {
        Command::assertValidValue($action);
        switch ($action) {
            case Command::TURN_LEFT:
                return $this->robot->turnLeft();
            case Command::TURN_RIGHT:
                return $this->robot->turnRight();
            case Command::ADVANCE:
            case Command::BACK:
                return $this->move($action);
            case Command::CLEAN:
                return $this->clean();
            default:
                return false;
        }
    }

    /**
     * @param string $command
     *
     * @return bool
     * @throws BatteryLowException
     */
    private function move(string $command): bool
    {
        Command::assertValidValue($command);
        $moveSet = (new Facing($this->robot->getPosition()->getFacing()))->getMoveSet($command);
        $nextX = $this->robot->getPosition()->getX() + $moveSet[0];
        $nextY = $this->robot->getPosition()->getY() + $moveSet[1];
        $cell = $this->config->getCell($nextX, $nextY);
        if ($this->robot->move($cell, $command) === true) {
            $this->result->addVisited($cell);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws BatteryLowException
     */
    private function clean(): bool
    {
        $cell = $this->config->getCell($this->robot->getPosition()->getX(), $this->robot->getPosition()->getY());
        if ($this->robot->clean() === true) {
            $this->result->addCleaned($cell);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws BatteryLowException
     * @throws StuckException
     */
    private function startBackoffStrategy(): bool
    {
        $this->logger->warning('Performing backoff strategy', [$this->robot]);

        foreach (Robot::BACKOFF_STRATEGY as $stepId => $step) {
            $failed = false;
            foreach ($step as $command) {
                if (!$this->performAction($command)) {
                    $failed = true;
                    break;
                }
            }
            if (!$failed) {
                $this->logger->warning(sprintf("Strategy successful at step %d", $stepId + 1));

                return true;
            }
        }
        $this->logger->warning('Strategy failed');
        throw new StuckException('The robot is considered stuck');
    }
}
