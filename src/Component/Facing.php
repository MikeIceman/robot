<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\FacingInterface;
use MyCLabs\Enum\Enum;

/**
 * Class Facing
 *
 * @package App\Component
 */
class Facing extends Enum implements FacingInterface
{
    public const NORTH = 'N';

    public const EAST = 'E';

    public const SOUTH = 'S';

    public const WEST = 'W';

    /**
     * @param string $command
     *
     * @return int[]
     */
    public function getMoveSet(string $command): array
    {
        Command::assertValidValue($command);
        $moveSets = $this->getMoveSets();
        $moveSet = $moveSets[$this->value] ?? [0, 0];
        if ($command === Command::ADVANCE) {
            return $moveSet;
        }

        return [$moveSet[0] * -1, $moveSet[1] * -1];
    }

    /**
     * Moves set
     *
     * @return int[][]
     */
    private function getMoveSets(): array
    {
        return [
            static::NORTH => [0, -1],
            static::EAST => [1, 0],
            static::SOUTH => [0, 1],
            static::WEST => [-1, 0],
        ];
    }
}
