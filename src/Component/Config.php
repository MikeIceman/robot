<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\ConfigInterface;

/**
 * Class Config
 *
 * @package App\Component
 */
class Config implements ConfigInterface
{
    /** @var array */
    private array $map = [];

    /** @var Position */
    private Position $start;

    /** @var array */
    private array $commands = [];

    /** @var int */
    private int $battery = 0;

    public function __construct()
    {
        $this->start = new Position();
    }

    /**
     * @param int $x
     * @param int $y
     *
     * @return Cell
     */
    public function getCell(int $x, int $y): Cell
    {
        return new Cell($this->map[$y][$x] ?? null, $x, $y);
    }

    /**
     * @return Position
     */
    public function getStart(): Position
    {
        return $this->start;
    }

    /**
     * @return string[]
     */
    public function getCommands(): array
    {
        return $this->commands;
    }

    /**
     * @return int
     */
    public function getBattery(): int
    {
        return $this->battery;
    }
}
