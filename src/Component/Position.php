<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\PositionInterface;

/**
 * Class Position
 *
 * @package App\Component
 */
class Position implements PositionInterface
{
    /** @var int */
    public int $X = 0;

    /** @var int */
    public int $Y = 0;

    /** @var string */
    public string $facing;

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->X;
    }

    /**
     * @param int $X
     */
    public function setX(int $X): void
    {
        $this->X = $X;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->Y;
    }

    /**
     * @param int $Y
     */
    public function setY(int $Y): void
    {
        $this->Y = $Y;
    }

    /**
     * @return string
     */
    public function getFacing(): string
    {
        return $this->facing;
    }

    /**
     * @param string $facing
     */
    public function setFacing(string $facing): void
    {
        $this->facing = $facing;
    }
}
