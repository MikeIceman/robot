<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\ResultInterface;

/**
 * Class Result
 *
 * @package App\Component
 */
class Result implements ResultInterface
{
    /** @var Cell[] */
    public array $visited = [];

    /** @var Cell[] */
    public array $cleaned = [];

    /** @var Position */
    public Position $final;

    /** @var int */
    public int $battery = 0;

    /**
     * @param Position $final
     */
    public function setFinal(Position $final): void
    {
        $this->final = $final;
    }

    /**
     * @param int $battery
     */
    public function setBattery(int $battery): void
    {
        $this->battery = $battery;
    }

    /**
     * @param Cell $cell
     */
    public function addVisited(Cell $cell): void
    {
        $this->visited[] = $cell;
    }

    /**
     * @param Cell $cell
     */
    public function addCleaned(Cell $cell): void
    {
        $this->cleaned[] = $cell;
    }
}
