<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\RobotInterface;
use App\Exception\BatteryLowException;
use App\Exception\ValidationException;

/**
 * Class Robot
 *
 * @package App\Component
 */
class Robot implements RobotInterface
{
    /** @var Position */
    private Position $position;

    /** @var int */
    private int $battery = 0;

    /** @var array[] */
    public const TURN_STRATEGY = [
        Command::TURN_LEFT => [
            Facing::NORTH => Facing::WEST,
            Facing::EAST => Facing::NORTH,
            Facing::SOUTH => Facing::EAST,
            Facing::WEST => Facing::SOUTH,
        ],
        Command::TURN_RIGHT => [
            Facing::NORTH => Facing::EAST,
            Facing::EAST => Facing::SOUTH,
            Facing::SOUTH => Facing::WEST,
            Facing::WEST => Facing::NORTH,
        ],
    ];

    /** @var array[] */
    public const BACKOFF_STRATEGY = [
        [Command::TURN_RIGHT, Command::ADVANCE, Command::TURN_LEFT,],
        [Command::TURN_RIGHT, Command::ADVANCE, Command::TURN_RIGHT,],
        [Command::TURN_RIGHT, Command::ADVANCE, Command::TURN_RIGHT,],
        [Command::TURN_RIGHT, Command::BACK, Command::TURN_RIGHT, Command::ADVANCE,],
        [Command::TURN_LEFT, Command::TURN_LEFT, Command::ADVANCE, ],
    ];

    /**
     * @return Position
     */
    public function getPosition(): Position
    {
        return $this->position;
    }

    /**
     * @param Position $position
     */
    public function setPosition(Position $position): void
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getBattery(): int
    {
        return $this->battery;
    }

    /**
     * @param int $battery
     *
     * @throws ValidationException
     */
    public function setBattery(int $battery): void
    {
        if ($battery < 0) {
            throw new ValidationException('Battery level can not be negative');
        }
        $this->battery = $battery;
    }

    /**
     * @param int $value
     *
     * @throws BatteryLowException
     */
    public function consume(int $value): void
    {
        if ($this->battery < $value) {
            throw new BatteryLowException('Battery level is too low');
        }
        $this->battery -= $value;
    }

    /**
     * Turns robot to left
     *
     * @throws BatteryLowException
     */
    public function turnLeft(): bool
    {
        $this->consume(Command::getCost(Command::TURN_LEFT));
        return $this->turn(Command::TURN_LEFT);
    }

    /**
     * Turns robot to left
     *
     * @throws BatteryLowException
     */
    public function turnRight(): bool
    {
        $this->consume(Command::getCost(Command::TURN_RIGHT));
        return $this->turn(Command::TURN_RIGHT);
    }

    /**
     * @param Cell $cell
     * @param string $command
     *
     * @return bool
     * @throws BatteryLowException
     */
    public function move(Cell $cell, string $command): bool
    {
        Command::assertValidValue($command);
        $this->consume(Command::getCost($command));
        if ($cell->getType() !== Cell::CLEANABLE) {
            return false;
        }
        $this->position->setX($cell->getX());
        $this->position->setY($cell->getY());
        return true;
    }

    /**
     * @return bool
     * @throws BatteryLowException
     */
    public function clean(): bool
    {
        $this->consume(Command::getCost(Command::CLEAN));
        return true;
    }

    /**
     * @param string $command
     *
     * @return bool
     */
    private function turn(string $command): bool
    {
        if (isset(static::TURN_STRATEGY[$command][$this->position->facing])) {
            $this->position->setFacing(static::TURN_STRATEGY[$command][$this->position->facing]);
            return true;
        }
        return false;
    }
}
