<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\CellInterface;

/**
 * Class Cell
 *
 * @package App\Component
 */
class Cell implements CellInterface
{
    public const CLEANABLE = 'S';

    // Those properties are not really used yet
    // public const BLOCKED = 'C';
    // public const WALL = null;

    /** @var int */
    public int $X = 0;

    /** @var int */
    public int $Y = 0;

    /** @var null|string  */
    private ?string $type = null;

    public function __construct($value, int $x, int $y)
    {
        $this->setX($x);
        $this->setY($y);
        $this->setType($value);
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->X;
    }

    /**
     * @param int $X
     */
    public function setX(int $X): void
    {
        $this->X = $X;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->Y;
    }

    /**
     * @param int $Y
     */
    public function setY(int $Y): void
    {
        $this->Y = $Y;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }
}
