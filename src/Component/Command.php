<?php

declare(strict_types=1);

namespace App\Component;

use App\Domain\Component\CommandInterface;
use MyCLabs\Enum\Enum;

/**
 * Class Command
 *
 * @package App\Component
 */
class Command extends Enum implements CommandInterface
{
    public const TURN_LEFT = 'TL';

    public const TURN_RIGHT = 'TR';

    public const ADVANCE = 'A';

    public const BACK = 'B';

    public const CLEAN = 'C';

    /**
     * @return int[]
     */
    private static function consumes(): array
    {
        return [
            static::TURN_LEFT => 1,
            static::TURN_RIGHT => 1,
            static::ADVANCE => 2,
            static::BACK => 3,
            static::CLEAN => 5,
        ];
    }

    /**
     * @param string $command
     *
     * @return int
     */
    public static function getCost(string $command): int
    {
        self::assertValidValue($command);
        return static::consumes()[$command];
    }
}
