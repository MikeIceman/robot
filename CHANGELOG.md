# Changelog

All notable changes to `MikeIceman/myq` will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [v0.2.3] - 2021-02-19
### Added
- Some simple tests added

## [v0.2.2] - 2021-02-19
### Changed
- Code cleanup and refactoring
- Docker annotations added to readme

## [v0.2.1] - 2021-02-19
### Removed
- Remove redundant CI config

### Changed
- Code cleanup
- Robot turn strategies added

## [v0.2.0] - 2021-02-18
### Fixed
- Use ParameterBag instead of Container in FileStorage

### Added
- Docker container support

## [v0.1.1] - 2021-02-18
### Fixed
- Fix missing logger

## [v0.1.0] - 2021-02-18
### Changed
- Parser helper splitted to file storage service and config factory

## [v0.0.3] - 2021-02-18
## Changed
- Simulation service refactoring and code cleanup

## [v0.0.2] - 2021-02-18
### Fixed
- Fixed typos in readme
- Remove redundant setDescription() in command definition
- Code cleanup

### Removed
- Redundant methods

## [v0.0.1] - 2021-02-18
Project release
