# MyQ Programming test

[![PHP version](https://img.shields.io/badge/php-%5E7.4-blue)](https://php.com)
[![Symfony](https://img.shields.io/badge/Powered_by-Symfony_Framework-green.svg?style=flat)](https://symfony.com/)
[![pipeline status](https://gitlab.com/MikeIceman/robot/badges/master/pipeline.svg)](https://gitlab.com/MikeIceman/robot/-/commits/master)
[![coverage report](https://gitlab.com/MikeIceman/robot/badges/master/coverage.svg)](https://gitlab.com/MikeIceman/robot/-/commits/master)

**WARNING**: This is only a demo application! **Viewer discretion is advised.** 

## What's inside?
**Robot** is a simple demo application that provides console command to simulate robot actions provided by input config file

## Getting started
The easiest way to get latest version is to clone git repository:

```shell
git clone git@gitlab.com:MikeIceman/robot.git
```

## Installation and usage
Go to cloned project and install composer dependencies. Run:

```shell
cd robot
composer install
```

That's all! Now you can run simulation command:

```shell
php bin/console simulation:start input.json output.json
```

By default input and output files are stored in [storage](storage) directory.

You can configure another storage directory in [services.yaml](config/services.yaml):

```yaml
# config/services.yaml

parameters:
    storage_directory: '/tmp/my_own_storage'
```

## Running tests

There were two commands to run tests:

1. Simply run tests without generating of coverage report:
```shell
composer test
```
2. Run tests with coverage report:
```shell
composer test-coverage
```

## Another options

You can also use some built-in instruments:
1. Check code style according to PSR standards
```shell
composer check-cs
```

2. Automatically fix code style according to previous recommendations
```shell
composer fix-cs
```

## Working with Docker images
You can easily run docker container by using following command:
```shell
docker-compose up -d --build
```
This command starts three containers:
- Nginx
- PHP-FPM
- MySQL database

So now we have complete Symfony 5.4 application at our [localhost](http://localhost) without using local webserver

## TODO:
- Probably check for output file existence (removed in first version)
- Add website part to upload and execute commands via web interface
